# AES-CBC Authenticated Encryption/Decryption Tool

This tool implements AES in Cipher Block Chaining (CBC) mode with authenticated encryption and decryption by generating Message Authentication Codes (HMACs) using the Hash-based Message Authentication Code (HMAC). 
The tool takes files and keys as inputs and returns the encryption/decryption of the corresponding file as well as a message demonstrating whether the file was actually encrypted/decrypted with the supplied key. Therefore, it operates in exactly two modes, encrypt and decrypt.

## Build and Usage Instructions

After extracting the source code, build it as follows: 
```
$ cd encrypt-auth
$ go build
```
The "encrypt-auth" binary will be built at your present working directory. This can now be used to encrypt and decrypt files as follows:
#### To Encrypt:
```
$ ./encrypt-auth encrypt -k AABBCCDDEEFF10100101FFEEDDCCBBAAAABBCCDDEEFF10100101FFEEDDCCBBAA -i input.txt -o output.enc
```
#### To Decrypt:
```
$ ./encrypt-auth decrypt -k AABBCCDDEEFF10100101FFEEDDCCBBAAAABBCCDDEEFF10100101FFEEDDCCBBAA -i output.enc -o output.txt
```
Note that the key ```AABBCCDDEEFF10100101FFEEDDCCBBAAAABBCCDDEEFF10100101FFEEDDCCBBAA``` consists of a 64-byte string of all hex characters. Under the hood, this is converted to a 32-byte string using the ```encoding/hex.Decode``` package of golang.

## Author
* Purushottam Kulkarni
* pkulkar6@jhu.edu
