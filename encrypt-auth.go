package main

import ("fmt"
	"io/ioutil"
	"strings"
	"os"
	"crypto/rand"
	"crypto/sha256"
	"crypto/aes"
	"encoding/hex"
)

type Flags struct {

	Mode string
	Key []byte
	InputFile string
	OutputFile string
}

func check(e error) {
	if e != nil {
		fmt.Println("\033[91m[!]\033[0m ERROR:",e, "\n")
		show_usage()
		os.Exit(1)
	}
}

func show_usage() {
	fmt.Println("\033[94m[*]\033[0m Authenticated Encryption Tool \033[94m[*]\033[0m")
	fmt.Println("A tool for performing AES-128 CBC encryption/decryption and signing using HMAC\n")
	fmt.Println("usage: ./encrypt-auth <mode> -k <32-byte key> -i <input-file> -o <output-file>\n")
	fmt.Println("Flag\tMeaning")
	fmt.Println("----------------------------------------------------------------")
	fmt.Println("<mode>\tMode of operation i.e. encrypt/decrypt")
	fmt.Println("-k\tKey to be used for the encrypt/decrypt operation and signing")
	fmt.Println("-i\tInput File to be encrypted/decrypted")
	fmt.Println("-o\tOutput File containing result of encryption/decryption\n")
}

func flagParse(args []string) *Flags {

	flags := new(Flags)

	if strings.Compare(args[1], "encrypt")!=0 && strings.Compare(args[1], "decrypt")!=0 {
		show_usage()
		os.Exit(1)
	}
	flags.Mode = args[1]
	for i, flag := range args[2:] {
		if i%2 != 0 {
			if strings.Compare(args[2+i-1], "-k") == 0 {
				src := []byte(flag)
				if hex.DecodedLen(len(src)) != 32 {
					fmt.Println("\033[91m[!]\033[0m ERROR: Key must be 32 bytes in length\n")
					show_usage()
					os.Exit(1)
				}
				dst := make([]byte, hex.DecodedLen(len(src)))
				_, err := hex.Decode(dst, src)
				check(err)
				flags.Key = dst
			} else if strings.Compare(args[2+i-1], "-i") == 0 {
				flags.InputFile = flag
			} else if strings.Compare(args[2+i-1], "-o") == 0 {
				flags.OutputFile = flag
			} else {
				show_usage()
				os.Exit(1)
			}
		}
	}

	return flags
}

func block_exor(data, iv []byte) []byte {

	result := make([]byte, len(iv))

	for i:=0;i<len(iv);i++ {
		result[i] = data[i] ^ iv[i]
	}

	return result
}

func remove_pad(data []byte) []byte {

	n := data[len(data)-1]
	var pad_len int

	if n == aes.BlockSize {
		pad_len = aes.BlockSize
		fmt.Println(pad_len)
	} else {
		pad_len = int(n)
		fmt.Println(pad_len)
	}

	for i:=1;i<=pad_len;i++ {
		if data[len(data)-i] != byte(n) {
			fmt.Println("INVALID PADDING")
			os.Exit(0)
		}
	}

	unpadded_data := data[0:(len(data)-int(pad_len))]

	return unpadded_data
}

func calc_hmac(data, key []byte) [32]byte {

	var hmac_key []byte
	ipad := make([]byte, sha256.BlockSize)
	opad := make([]byte, sha256.BlockSize)

	for i:=0;i<sha256.BlockSize;i++ {
		ipad[i] = byte(0x36)
		opad[i] = byte(0x5c)
	}

	if len(key) == sha256.BlockSize {
		hmac_key = key

	} else if len(key) < sha256.BlockSize {
		hmac_key = key
		for i:=0;i<(sha256.BlockSize-len(key));i++ {
			hmac_key = append(hmac_key, byte(0x00))
		}

	} else {

		key_sum := sha256.Sum256(key)
		for i:=0;i<(len(key_sum));i++ {
			hmac_key = append(hmac_key, key_sum[i])
		}
		for i:=0;i<(sha256.BlockSize-len(key_sum));i++ {
			hmac_key = append(hmac_key, byte(0x00))
		}
	}

	k_ipad := block_exor(ipad, hmac_key)
	k_ipad = append(k_ipad, data...)

	hash_k_ipad := sha256.Sum256(k_ipad)

	k_opad := block_exor(opad, hmac_key)
	for i:=0;i<len(hash_k_ipad);i++ {
		k_opad = append(k_opad, hash_k_ipad[i])
	}

	hmac := sha256.Sum256(k_opad)

	return hmac
}

func check_mac(data, data_hmac, sign_key []byte) {

	hmac := calc_hmac(data, sign_key)

	if len(hmac) != len(data_hmac) {
		fmt.Println("INVALID MAC")
		os.Exit(0)
	}
	for i:=0;i<len(hmac);i++ {
		if data_hmac[i] != hmac[i] {
			fmt.Println("INVALID MAC")
			os.Exit(1)
		}
	}

	fmt.Println("\033[92m[+]\033[0m MAC Verified, message is authentic. ")
}

func aes_auth_encrypt(plaintext, key []byte) []byte {

	var ciphertext []byte
	iv := make([]byte, aes.BlockSize)
	ct := make([]byte, aes.BlockSize)

	enc_key := key[0:aes.BlockSize]
	sign_key := key[aes.BlockSize:len(key)]

	hmac := calc_hmac(plaintext, sign_key)
	fmt.Println("\033[92m[+]\033[0m HMAC Calculated with Signing Key")
	for i:=0;i<len(hmac);i++ {
		plaintext = append(plaintext, hmac[i])
	}

	ecb_cipher, err := aes.NewCipher(enc_key)
	check(err)

	rand.Read(iv)

	ciphertext = append(ciphertext, iv...)

	if len(plaintext)%aes.BlockSize != 0 {
		pad_length := aes.BlockSize - (len(plaintext)%aes.BlockSize)
		for i:=0;i<pad_length;i++ {
			plaintext = append(plaintext, byte(pad_length))
		}
	} else {
		for i:=0;i<aes.BlockSize;i++ {
			plaintext = append(plaintext, byte(aes.BlockSize))
		}
	}

	blocks := len(plaintext)/aes.BlockSize

	for i:=0;i<blocks;i++ {

		curr_block := block_exor(plaintext[(i*aes.BlockSize):(i+1)*aes.BlockSize], iv)
		ecb_cipher.Encrypt(ct, curr_block)
		iv = ct
		ciphertext = append(ciphertext, ct...)
	}

	return ciphertext
}

func aes_auth_decrypt(ciphertext, key []byte) []byte {

	var plaintext []byte
	pt := make([]byte, aes.BlockSize)
	iv := make([]byte, aes.BlockSize)

	dec_key := key[0:aes.BlockSize]
	sign_key := key[aes.BlockSize:len(key)]

	iv = ciphertext[0:aes.BlockSize]
	ecb_cipher, err := aes.NewCipher(dec_key)
	check(err)
	blocks := (len(ciphertext)/aes.BlockSize)

	for i:=1;i<blocks;i++ {

		curr_block := ciphertext[(i*aes.BlockSize):(i+1)*aes.BlockSize]
		ecb_cipher.Decrypt(pt, curr_block)
		temp := block_exor(pt, iv)
		iv = curr_block

		plaintext = append(plaintext, temp...)
	}

	unpadded_pt := remove_pad(plaintext)
	data_hmac := unpadded_pt[(len(unpadded_pt)-sha256.Size):]
	real_pt := unpadded_pt[0:(len(unpadded_pt)-sha256.Size)]

	check_mac(real_pt, data_hmac, sign_key)

	return real_pt
}


func main() {


	if len(os.Args) < 8 || len(os.Args) > 8 {
		show_usage()
		os.Exit(0)
	}

	flags := flagParse(os.Args)

	data, err := ioutil.ReadFile(flags.InputFile)
	check(err)

	if strings.Compare(flags.Mode, "encrypt") == 0 {

		ciphertext := aes_auth_encrypt(data, flags.Key)
		err_e := ioutil.WriteFile(flags.OutputFile, ciphertext, 0666)
		check(err_e)
		fmt.Println("\033[92m[+]\033[0m Encryption and Signing Successful! Output written to file", flags.OutputFile)

	} else if strings.Compare(flags.Mode, "decrypt") == 0 {

		plaintext := aes_auth_decrypt(data, flags.Key)
		err_d := ioutil.WriteFile(flags.OutputFile, plaintext, 0666)
		check(err_d)
		fmt.Println("\033[92m[+]\033[0m Decryption Successful! Output written to file", flags.OutputFile)

	} else {
		show_usage()
	}


}
